package com.faceme.diagnostics;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@SpringBootApplication
public class DiagnosticsStatisticsConsumerApplication {

	@Bean
    ConnectionFactory connectionFactory() {
	    return new CachingConnectionFactory("localhost");
    }

    @Bean
    AbstractMessageListenerContainer listenerContainer() {
        SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer(connectionFactory());
        listenerContainer.setMessageListener(new MessageListenerAdapter(new Consumer()));
        listenerContainer.setQueueNames("myQueue");
        listenerContainer.start();
	    return listenerContainer;
    }

    @PostConstruct
    public void init() {
        listenerContainer().start();
    }

	public static void main(String[] args) {SpringApplication.run(DiagnosticsStatisticsConsumerApplication.class, args);}
}
