package com.faceme.diagnostics;

import org.springframework.amqp.core.Message;

public interface MessageDelegate {
    void handleMessage(String message);
}
