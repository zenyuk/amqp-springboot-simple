package com.faceme.diagnostics;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.inject.Inject;

@Component
@EnableScheduling
public class Emitter {
    @Inject
    RabbitTemplate rabbitTemplate;

    @Scheduled(fixedDelay = 5_000)
    public void emit() {
        System.out.println("============ emit =======================================");

        rabbitTemplate.convertAndSend("first msg");
        rabbitTemplate.convertAndSend("second msg");
    }
}
